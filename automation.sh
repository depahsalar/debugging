#!/bin/sh
LATEST_SCRIPT_HOME="./latest_generated/";
BACKUP_SCRIPT_HOME="./backups/automation_scripts/";
REMOTE_SCRIPT_HOME="/ltapps/initscripts/";
METADATA_BACKUP_HOME="./backups/";
#### METADATA_HOME SHOULD NOT BE IN THE SAME DIRECTORY AS THE EXECUTION DIRECTORY #########
METADATA_HOME="/ltapps/automation/automation_unix_source/temp_metadata/";
logDateTime=$(date +"%Y%m%d%H%M%S")
logValue="./logs/automation.$logDateTime.log"
username=""
password=""
environment=""

dynamic_script_builder()
{
 log Creating $1
 dynamic_script_default $1
 log ApplicationList $1
 cat $METADATA_HOME/$application_metadata | grep "$1" | while read application;
 do
  application_detail=($application)
  log ApplicationName ${application_detail[2]}
  echo -e $application >> $LATEST_SCRIPT_HOME$1
 done
 log Created $1
}

dynamic_script_default()
{
 file="$LATEST_SCRIPT_HOME$1"
 if [ -f $file ]
 then
  log BackUp $1
  mv $LATEST_SCRIPT_HOME$1 $BACKUP_SCRIPT_HOME$1.$(date +"%Y%m%d%H%M%S")
 fi

 touch $LATEST_SCRIPT_HOME$1
}

update_on_remoteserver()
{
 log Updating $1
 #chmod 644 $LATEST_SCRIPT_HOME$1

 if sshpass -p $password ssh $username@$1 test -e "/ltapps/initscripts/application_metadata"; then
    sshpass -p $password ssh $username@$1 rm -f /ltapps/initscripts/application_metadata
 fi
 if sshpass -p $password ssh $username@$1 test -e "/ltapps/initscripts/initscript.sh"; then
    sshpass -p $password ssh $username@$1 rm -f /ltapps/initscripts/initscript.sh
 fi
 if sshpass -p $password ssh $username@$1 test -e "/ltapps/initscripts/auto_deploy_old_tomcat.sh"; then
    sshpass -p $password ssh $username@$1 rm -f /ltapps/initscripts/auto_deploy_old_tomcat.sh
 fi
 if sshpass -p $password ssh $username@$1 test -e "/ltapps/initscripts/auto_deploy_gia"; then
    sshpass -p $password ssh $username@$1 rm -f /ltapps/initscripts/auto_deploy_gia.sh
 fi
 if sshpass -p $password ssh $username@$1 test -e "/ltapps/initscripts/auto_deploy_toads.sh"; then
    sshpass -p $password ssh $username@$1 rm -f /ltapps/initscripts/auto_deploy_toads.sh
 fi

 sshpass -p $password scp -o StrictHostKeyChecking=no $LATEST_SCRIPT_HOME$1 $username@$1.lifetouch.net:$REMOTE_SCRIPT_HOME"application_metadata"
 sshpass -p $password scp -o StrictHostKeyChecking=no core_initscript.sh $username@$1.lifetouch.net:$REMOTE_SCRIPT_HOME"initscript".sh
 sshpass -p $password scp -o StrictHostKeyChecking=no auto_deploy_old_tomcat.sh $username@$1.lifetouch.net:$REMOTE_SCRIPT_HOME"auto_deploy_old_tomcat".sh
 sshpass -p $password scp -o StrictHostKeyChecking=no auto_deploy_gia.sh $username@$1.lifetouch.net:$REMOTE_SCRIPT_HOME"auto_deploy_gia".sh
 sshpass -p $password scp -o StrictHostKeyChecking=no auto_deploy_toads.sh $username@$1.lifetouch.net:$REMOTE_SCRIPT_HOME"auto_deploy_toads".sh
 sshpass -p $password ssh $username@$1 chmod g+w /ltapps/initscripts/application_metadata
 sshpass -p $password ssh $username@$1 chmod g+wx /ltapps/initscripts/initscript.sh
 sshpass -p $password ssh $username@$1 chmod g+wx /ltapps/initscripts/auto_deploy_old_tomcat.sh
 sshpass -p $password ssh $username@$1 chmod g+wx /ltapps/initscripts/auto_deploy_gia.sh
 sshpass -p $password ssh $username@$1 chmod g+wx /ltapps/initscripts/auto_deploy_toads.sh
 log Updated $1
}

log()
{

if [ "$1" == "Creating" ]
then
 log
 echo "                                    $2                                      " >> $logValue
 log
 echo "Creating initscript.sh for "$2 >> $logValue
elif [ "$1" == "Created" ]
then
 echo "Created initscript.sh for "$2 >> $logValue
 log
elif [ "$1" == "Updating" ]
then
 echo "Updating initscript.sh on "$2 >> $logValue
elif [ "$1" == "Updated" ]
then
 echo "Updated initscript.sh on "$2" at "$REMOTE_SCRIPT_HOME >> $logValue
 log
elif [ "$1" == "ApplicationList" ]
then
 echo "Applications getting added in initscript.sh for server "$2" are shown below" >> $logValue
elif [ "$1" == "ApplicationName" ]
then
 echo "          "$2 >> $logValue
elif [ "$1" == "BackUp" ]
then
 echo "Taking backup of old automation script of "$2" on this server" >> $logValue
elif [ "$1" == "ErrorAll" ] || [ "$1" == "help" ]
then
 echo "================================================================" >> $logValue
 echo "Please enter all required parameters deliminated by space" >> $logValue
 echo "Parameter 1: ServerName or ALL" >> $logValue
 echo "***************************************************************" >> $logValue
 echo "Example 1: ./automation.sh ALL" >> $logValue
 echo "Example 2: ./automation.sh dvlapp04" >> $logValue
 echo "***************************************************************" >> $logValue
 echo "================================================================" >> $logValue
elif [ "$1" == "IncorrectServer" ]
then
 log
 echo "ERROR!!!!Server "$2" does not exist in application_metadata" >> $logValue
 log
else
 echo "=====================================================================================" >> $logValue
fi
}

if [ $# -lt 1 ] || [ "$1" == "help" ]; then
 log "ErrorAll"
else
 read -p "Environment (dev/qa/prod) :- " environment
 read -p "Enter Username :- " username
 read -s -p "Enter Password :- " password

 server_metadata=$environment"_server_metadata"
 application_metadata=$environment"_application_metadata"

 if [ -f $application_metadata ]; then
  mv $application_metadata $METADATA_BACKUP_HOME/$application_metadata.$(date +"%Y%m%d%H%M%S")
 fi

 if [ -f $server_metadata ]; then
  mv $server_metadata $METADATA_BACKUP_HOME/$server_metadata.$(date +"%Y%m%d%H%M%S")
 fi
 touch $server_metadata
 grep -Eo '^[^ ]+' $METADATA_HOME/$application_metadata | while read server ;
 do
  value=`cat $server_metadata | grep $server | wc -l`
  if [ $value -eq 0 ]; then
   echo -e "$server" >> $server_metadata
  fi
 done

 echo -e "\n\nProcessing Script Push, Logs will be shown at the end of processing\n"
 core_initscript=$LATEST_SCRIPT_HOME"core_initscript.sh"
 if [ -f $core_initscript ]; then
  log BackUp core_initscript.sh
  mv $LATEST_SCRIPT_HOME"core_initscript.sh" $BACKUP_SCRIPT_HOME"core_initscript.sh".$(date +"%Y%m%d%H%M%S")
 fi
 cp ./core_initscript.sh $LATEST_SCRIPT_HOME"core_initscript.sh"
 if [ "$1" == "ALL" ]; then
  cat $server_metadata | while read server ;
  do
   eval dynamic_script_builder $server
   update_on_remoteserver $server
   echo -n "."
  done
 else
  value=`cat $server_metadata | grep $1 | wc -l`
  if [ $value -eq 1 ]; then
   dynamic_script_builder $1
   update_on_remoteserver $1
  else
   log "IncorrectServer" $1
  fi
 fi
fi
echo -e "\n=====================================================================================\nCOMPLETED\n=====================================================================================\nScripts have been pushed to specified servers\n=====================================================================================\nFor more Information, open log file $logValue\n====================================================================================="
