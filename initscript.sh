#!/bin/bash
#set -x
function setmailer
{
  if [ "$ACTION" == "stop" ]; then
   echo "$1 application stopped"|tee -a /tmp/status
  fi
}
function toads
{
 cd $3
 if [ "$1" == "stop" ]; then
  setmailer $2
  $SWITCHUSERCMD $4 util/deploy.sh stop &
 elif [ "$1" == "start" ]; then
  $SWITCHUSERCMD $4 util/deploy.sh restart &
 fi
}
function nexus
{
 cd $3
 if [ "$1" == "stop" ]; then
  $SWITCHUSERCMD $4 ./nexus_lifetouch.sh stop &
  setmailer $2
 elif [ "$1" == "start" ]; then
  $SWITCHUSERCMD $4 ./nexus_lifetouch.sh start &
 fi
}
function old_tomcat
{
 cd $3
 if [ "$1" == "stop" ]; then
  $SWITCHUSERCMD $4 ./stop &
  echo "Sleep for 20 Seconds to Stop application completely"
  sleep 20
  [ -f $3/catalina.pid ] && rm -fr $3/catalina.pid
  setmailer $2
 elif [ "$1" == "start" ]; then
  $SWITCHUSERCMD $4 ./start &
 fi
}
function vertex
{
 cd $3
 if [ "$1" == "stop" ]; then
  $SWITCHUSERCMD $4 ./tomcat/bin/shutdown.sh &
  setmailer $2
 elif [ "$1" == "start" ]; then
  sleep 30
  $SWITCHUSERCMD $4 ./tomcat/bin/startup.sh &
 fi
}
function jira
{
 cd $3
 if [ "$1" == "stop" ]; then
  $SWITCHUSERCMD $4 ./bin/stop-jira.sh &
  setmailer $2
 elif [ "$1" == "start" ]; then
  $SWITCHUSERCMD $4 ./bin/start-jira.sh &
 fi
}
function svn
{
 cd $3
 if [ "$1" == "stop" ]; then
  $SWITCHUSERCMD $4 ./bin/csvn stop &
  setmailer $2
 elif [ "$1" == "start" ]; then
  $SWITCHUSERCMD $4 ./bin/csvn start &
 fi
}
function fisheye
{
 cd $3
 if [ "$1" == "stop" ]; then
  $SWITCHUSERCMD $4 ./fisheyectl_lifetouch.sh stop &
  setmailer $2
 elif [ "$1" == "start" ]; then
  $SWITCHUSERCMD $4 ./fisheyectl_lifetouch.sh start &
 fi
}
function gia
{
 cd $3
 if [ "$1" == "stop" ]; then
  $SWITCHUSERCMD $4 ./tomcat.sh stop latest &
  echo "Sleep for 20 Seconds to Stop application completely"
  sleep 20
  [ -f $3/catalina.pid ] && rm -fr $3/catalina.pid
  setmailer $2
 elif [ "$1" == "start" ]; then
  $SWITCHUSERCMD $4 ./tomcat.sh start latest &
 fi
}

status=""
function validate
{
 status="NO HEALTH CHECK URL ENTRY FOUND FOR THE APP!!"
 if [ "$1" != "" ]; then
  echo `curl -Is $1 | head -n1` > ./tmpstatus
  status=`cat ./tmpstatus | tr -d "\t\n\r"`
  if [ "$status" == "" ]; then
   output="ERROR!URL Validation Errored out, Please make sure application_metadata entry is correct on central server, MUST Check Health Page, "
  else
   echo "$status" > tmpstatus
   status=`cut -d' ' -f3 ./tmpstatus`
   if [ "$status" == "OK" ]; then
    output=" and HTTP Status is OK,"
   else
    output=" ,MUST Check Health Page,"
   fi
  fi
  status="$output For More Information, go to URL $1"
 fi
}

function stopapps
{
cat $METADATA_HOME/application_metadata | grep "$1" | while read application; do
        application_detail=($application)
        if [ "${application_detail[4]}" != "toads" ] || [ "$ACTION" != "start" ]
        then
         eval ${application_detail[4]} "stop" ${application_detail[2]} ${application_detail[3]} ${application_detail[5]}
        fi
done
}

function startapps
{
count=1
cat $METADATA_HOME/application_metadata | grep "$1" | while read application; do
        application_detail=($application)
        stopapps ${application_detail[2]}
        eval ${application_detail[4]} "start" ${application_detail[2]} ${application_detail[3]} ${application_detail[5]}
        echo "Waiting 90 seconds for ${application_detail[2]} to startup"
        sleep 90
        ps -ef | grep ${application_detail[3]} | grep -v grep > /dev/null && echo -e "\n$count -> ${application_detail[2]} Successfully started$status"|tee -a /tmp/status || echo -e "\n$count -> Warning, ${application_detail[2]} is not running!$status"|tee -a /tmp/status
        count=`expr $count + 1`
done
}

function cleanup
{
# clear tmp files and arrays
[ -f /tmp/status ] && rm /tmp/status
}

function mailer
{
MAILBODY="started"
if [ "$1" == "stop" ]; then
 MAILBODY="stopped"
fi
STATUS=`cat /tmp/status`
cat<<EOF | mailx -s "Applications on $HOSTNAME have been "$MAILBODY runteam2@lifetouch.com

Applications have been $MAILBODY on $HOSTNAME
########## NOTICE!!! ##############
The following applications have been $MAILBODY by $USER, using the /ltapps/initscripts/initscript.sh:

$STATUS

EOF
}

#####################
### Script starts here!

##### Setup Variables

METADATA_HOME=/ltapps/initscripts/
HOSTYPE=`uname -s`
if [ "$HOSTYPE" == "SunOS" ]; then
HOSTNAME=`hostname`
else
HOSTNAME=`hostname -s`
fi

USER=`who am i| awk '{print $1}'`
SWITCHUSERCMD="su"
if [ -z "$USER" ]; then
 SWITCHUSERCMD="su"
elif [ "$USER" == "root" ]; then
 SWITCHUSERCMD="su"
else
 SWITCHUSERCMD="sudo -u"
fi

ACTION="$1"
if [ "$1" == "stop" ]; then
 if [ "$#" -eq 2 ]; then
  stopapps $2
 else
  stopapps
 fi
elif [ "$1" == "start" ]; then
 if [ "$#" -eq 2 ]; then
  startapps $2
 else
  startapps
 fi
else
echo "Usage : initscript.sh start | stop"
fi
mailer $1
cleanup
